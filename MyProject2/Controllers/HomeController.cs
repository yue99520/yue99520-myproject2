﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyProject2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewData["one"] = 1;
            ViewData["two"] = 2;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult HtmlHelper()
        {
            return View();
        }

        public ActionResult razor()
        {
            return View();
        }
    }
}