﻿using MyProject2.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyProject2.Controllers
{
    public class BMIController : Controller
    {
        public ActionResult Index()
        {
            return View(new BMIData());
        }

        // GET: BMI
        [HttpPost]
        public ActionResult Index(BMIData bmiData)
        {

            if (ModelState.IsValid)
            {
                var result = bmiData.weight / (bmiData.height * bmiData.height);
                bmiData.BMI = (float)result;
                var s = "";
                if (result <= 18.5)
                {
                    s = "過輕";
                }
                else if (18.5 < result && result <= 24)
                {
                    s = "正常";
                }
                else if (24 < result && result <= 27)
                {
                    s = "過重";
                }
                else if (27 < result && result <= 30)
                {
                    s = "輕度肥胖";
                }
                else if (30 < result && result <= 35)
                {
                    s = "中度肥胖";
                }
                else
                {
                    s = "重度肥胖";
                }

                bmiData.level = (string)s;
            }
            return View(bmiData);
        }
    }
}