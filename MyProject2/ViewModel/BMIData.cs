﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MyProject2.ViewModel
{
    public class BMIData
    {
        [Display(Name = "體重"), 
            Required(ErrorMessage ="內容不可空"),
            RangeAttribute(30, 150, ErrorMessage = "體重請輸入30~150之間")]
        public float? weight { get; set; }

        [Display(Name = "身高"),
            Required(ErrorMessage = "內容不可空"),
            RangeAttribute(1, 2, ErrorMessage = "身高請輸入1~2之間")]
        public float? height { get; set; }
        public float BMI { get; set; }
        public string level{ get; set; }
    }
}